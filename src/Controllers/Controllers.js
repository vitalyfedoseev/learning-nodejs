/**
 * here we linking controllers and aliases
 */
const controllers = {
    default: require('./Index'),
    users: require('./Users'),
    error: require('./Error')
}

module.exports = controllers;