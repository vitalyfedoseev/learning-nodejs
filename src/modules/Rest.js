const querystring = require('querystring');

/**
 * Class for work with rest like requests
 * In POST / PUT supported work with  json / x-www-form-urlencoded
 * In GET / DELETE supported getting params from get params
 */
class RestHandler {
    constructor(req, res, handler) {
        this.req = req;
        this.res = res;
        this.handler = handler;
    }

    init() {
        const answer = new Promise((resolve, reject) => {
            if (this.req.method === 'POST' || this.req.method === 'PUT') {
                const body = [];
                this.req.on('data', (chunk) => {
                    body.push(chunk);
                });
                this.req.on('end', () => {
                    const parsedBody = Buffer.concat(body).toString();
                    this.postHandler(parsedBody).then((data) => {
                        resolve(data);
                    })
                });
            } else if (this.req.method === 'GET' || this.req.method === 'DELETE') {
                this.getHandler().then((data) => {
                    resolve(data);
                })

            } else {
                reject();
            }
        });

        answer.then(
            (data) => {
                let contentType = !!data.contentType ? data.contentType : 'text/html';
                this.res.setHeader('Content-Type', contentType);
                if (!!data.result) {
                    for (let k in data.result) {
                        this.res.write(data.result[k]);
                    }
                }
                this.res.end();
            },
            () => {
                console.log('bad request');
                this.res.setHeader('Content-Type', 'text/html');
                this.res.statusCode = 409;
                this.res.write('wrong request');
                this.res.end();
            }
        );
    }

    /**
     *
     * @returns {Promise<unknown>}
     */
    getHandler () {
        const ans = new Promise((resolve, reject) => {
            let params = this.req.url.split('?');
            if (!!params[1]) {
                params = querystring.parse(params[1]);
            }
            this.handler.init(params).then(
                (data) => {
                    resolve(data);
                }
            )
        })
        return ans;
    }

    /**
     *
     * @param object body
     * @returns {Promise<unknown>}
     */
    postHandler (body) {
        const ans = new Promise((resolve, reject) => {
            let params = {};
            if (!!this.req.headers['content-type']) {
                if (this.req.headers['content-type'] === 'application/json') {
                    params = JSON.parse(body);
                } else if (this.req.headers['content-type'] === 'application/x-www-form-urlencoded') {
                    params = querystring.parse(body);
                } else if (this.req.headers['content-type'] === 'application/xml') {

                }
            }
            this.handler.init(params).then(
                (data) => {
                    resolve(data)
                }
            )
        })

        return ans;
    }
}


module.exports = RestHandler;