/**
 * Here we use structure where for every route we describe enabled methods
 * and for every method need to sign name of controller
 *
 */
const routes = {
    '/': {
        methods:{
            get:'default',
            post:'default'
        }
    },
    '/users': {
        methods:{
            get:'users',
            post:'users'
        }
    }
}

module.exports = routes;