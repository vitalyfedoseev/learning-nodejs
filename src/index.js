const http = require('http');
const conf = require('./Config/Config');
const controllers = require('./Controllers/Controllers');
const Rest = require('./modules/Rest');
const routes = require('./routes');

const server = http.createServer((req,res) => {
    let path = req.url;
    let method = req.method.toLowerCase();
    let handler = {};
    if (!!routes[path] && !!routes[path].methods[method]) {
        handler = new controllers[routes[path].methods[method]]();
    } else {
        handler = controllers.error;
    }
    let rest = new Rest(req, res, handler);
    rest.init();
});

server.listen(conf.port);
console.log(`server started on ${conf.port} port`);